package com.company;

import java.util.Arrays;
import java.util.List;

public class Password {
    private String password;
    public Password(String password){
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean checkPassword(User user){
        Password password = user.getPassword();
        String userPassword = password.getPassword();
        boolean haveDigit=false,
                haveUpperCase=false,
                haveLowerCase=false;
        if(userPassword.length() >= 9){
            for(int i=0;i<userPassword.length();i++){
                Character c = userPassword.charAt(i);
                if(c >= 48 || c <= 57 ){
                    haveDigit = true;
                }
                if(c >= 65 && c<=90){
                    haveUpperCase = true;
                }
                if(c >= 97 && c <= 122){
                    haveLowerCase = true;
                }
            }
            return haveDigit && haveLowerCase && haveUpperCase;
        }
        else{
            return false;
        }
    }
}
