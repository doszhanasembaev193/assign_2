package com.company;

import java.util.ArrayList;

public class Suits extends User {
    private String shoes;
    private String shirt;
    private ArrayList<Costume> costume;

    public Suits() {

    }
//12323112
    public Suits(String shoes, String shirt) {
        setShoes(shoes);
        setShirt(shirt);
        costume = new ArrayList<Costume>();
    }

    public Suits(String shoes, String shirt) {
        super(name, surname);
        setShoes(shoes);
        setShirt(shirt);
        costume = new ArrayList<Costume>();
    }

    public Suits(String name, String surname, String username, Password password, String shoes, String shirt) {
        super(name, surname, username, password);
        setShoes(shoes);
        setShirt(shirt);
        costume = new ArrayList<Costume>();
    }

    public void setShoes(String shoes) {
        this.shoes = shoes;
    }

    public String getShoes() {
        return shirt;
    }

    public void setShirt(String shirt) {
        this.shirt = shirt;
    }

    public String getShirt() {
        return shirt;
    }

    public void addCostume(Costume costume) {
        costume.add(costume);
    }

    public ArrayList<Costume> getCostumes() {
        return costume;
    }

    public String toString() {
        return super.toString() + " " + shoes + " " + shirt;
    }

}
