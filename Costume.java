package com.company;

public class Costume extends Suits {
    private String tuxedo;
    private int price;

    public Costume() {

    }

    public Costume(String tuxedo, int price) {
        setTuxedo(tuxedo);
        setPrice(price);
    }

    public Costume(String shoes, String shirt, String tuxedo, int price) {
        super(shoes, shirt);
        setTuxedo(tuxedo);
        setPrice(price);
    }

    public Costume(String name, String surname, String username, Password password, String shoes, String shirt, String tuxedo, int price) {
        super(name, surname, username, password, shoes, shirts);
        setTuxedo(tuxedo);
        setPrice(price);
    }

    public void setTuxedo(String tuxedo) {
        this.tuxedo = tuxedo;
    }

    public String getTuxedo() {
        return tuxedo;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return super.toString() + " " + tuxedo + " " + price;
    }

}
