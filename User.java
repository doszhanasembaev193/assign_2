package com.company;

import java.util.ArrayList;

public class User {
    private static int gen_id = 1;
    private int id;
    private String name;
    private String surname;
    private String username;
    private Password password;
    private ArrayList<Suits> suits;

    public User() {

    }

    public User(String name, String surname) {
        generateId();
        setName(name);
        setSurname(surname);
        suits = new ArrayList<suit>();
    }

    public User(String name, String surname, String username) {
        generateId();
        setName(name);
        setSurname(surname);
        setUsername(username);
        suits = new ArrayList<Suit>();
    }

    public User(String name, String surname, String username, Password password) {
        generateId();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        suits = new ArrayList<Suit>();
    }

    public User(int id, String name, String surname, String username, Password password) {
        this.id = id;
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        suits = new ArrayList<Suit>();
    }

    public void generateId() {
        id = gen_id++;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Password getPassword() {
        return password;
    }

    public void addSuit(Suit suit) {
        suits.add(suit);
    }

    public ArrayList<Suit> getSuit() {
        return suits;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password.getPasswordStr();
    }

}
